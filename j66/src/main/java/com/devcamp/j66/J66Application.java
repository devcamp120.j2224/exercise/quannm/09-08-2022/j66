package com.devcamp.j66;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J66Application {

	public static void main(String[] args) {
		SpringApplication.run(J66Application.class, args);
	}

}
