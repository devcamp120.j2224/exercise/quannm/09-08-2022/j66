package com.devcamp.j66.model;


import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "province")
public class CProvince {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int province_id;

	private String code;

	private String name;

    @OneToMany(mappedBy = "province", cascade = CascadeType.ALL)
    // @JsonManagedReference
    @JsonIgnore
	private Set<CDistrict> districts;

    public CProvince() {
    }

    public CProvince(int province_id, String code, String name) {
        this.province_id = province_id;
        this.code = code;
        this.name = name;
    }

    public int getId() {
        return province_id;
    }

    public void setId(int province_id) {
        this.province_id = province_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public Set<CDistrict> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<CDistrict> districts) {
        this.districts = districts;
    }
    
    
}
