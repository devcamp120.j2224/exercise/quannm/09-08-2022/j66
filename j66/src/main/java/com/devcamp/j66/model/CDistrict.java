package com.devcamp.j66.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "district")
public class CDistrict {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int district_id;

	private String name;

	private String prefix;

    @ManyToOne
    @JoinColumn(name = "province_id")
    // @JsonBackReference
	private CProvince province;

    @OneToMany(mappedBy = "district", cascade = CascadeType.ALL)
    // @JsonManagedReference
    @JsonIgnore
	private Set<CWard> wards;

    public CDistrict() {
    }

    public CDistrict(String name, String prefix, Set<CWard> wards) {
        this.name = name;
        this.prefix = prefix;
        this.wards = wards;
    }

    public int getId() {
        return district_id;
    }

    public void setId(int district_id) {
        this.district_id = district_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public CProvince getProvince() {
        return province;
    }

    public void setProvince(CProvince province) {
        this.province = province;
    }

    @JsonIgnore
    public Set<CWard> getWards() {
        return wards;
    }

    public void setWards(Set<CWard> wards) {
        this.wards = wards;
    }

    
}
