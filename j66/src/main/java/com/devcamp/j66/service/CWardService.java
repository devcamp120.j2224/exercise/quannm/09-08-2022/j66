package com.devcamp.j66.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.j66.model.CWard;
import com.devcamp.j66.repository.IWardRepository;

@Service
public class CWardService {
    @Autowired
    IWardRepository pIWardRepository;

    public List<CWard> getAllWard() {
        List<CWard> wards = new ArrayList<>();
        pIWardRepository.findAll().forEach(wards::add);
        return wards;
    }

    public CWard getWardById(int id) {
        Optional<CWard> wards = pIWardRepository.findById(id);
        if (wards.isPresent()) {
            CWard ward = wards.get();
            return ward;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> postNewWard(CWard wards) {
        Optional<CWard> wardData = pIWardRepository.findById(wards.getId());
        if(wardData.isPresent()) {
            return ResponseEntity.unprocessableEntity().body(" Ward already exsit  ");
        }
        CWard _ward = pIWardRepository.save(wards);
        return new ResponseEntity<>(_ward, HttpStatus.CREATED);
    }

    public CWard updateWardById(int id, CWard wards) {
        Optional<CWard> wardData = pIWardRepository.findById(id);
        if (wardData.isPresent()) {
			CWard _ward= wardData.get();
			_ward.setName(wards.getName());
			_ward.setDistrict(wards.getDistrict());
			_ward.setPrefix(wards.getPrefix());
            return _ward;
        } else {
            return null;
        }
    }

    public ResponseEntity<CWard> deleteWardById(int id) {
        try {
			pIWardRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    public ResponseEntity<CWard> deleteAllWard() {
		try {
			pIWardRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
