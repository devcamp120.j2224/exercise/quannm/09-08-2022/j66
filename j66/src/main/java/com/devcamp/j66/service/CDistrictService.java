package com.devcamp.j66.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.j66.model.CDistrict;
import com.devcamp.j66.repository.IDistrictRepository;

@Service
public class CDistrictService {
    @Autowired
    IDistrictRepository pDistrictRepository;

    public List<CDistrict> getAllDistrict() {
        List<CDistrict> districts = new ArrayList<>();
        pDistrictRepository.findAll().forEach(districts::add);
        return districts;
    }

    public CDistrict getDistrictById(int id) {
        Optional<CDistrict> dist = pDistrictRepository.findById(id);
        if (dist.isPresent()) {
            CDistrict district = dist.get();
            return district;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> postNewDistrict(CDistrict districts) {
        Optional<CDistrict> districtData = pDistrictRepository.findById(districts.getId());
        if(districtData.isPresent()) {
            return ResponseEntity.unprocessableEntity().body(" District already exsit  ");
        }
        CDistrict _district = pDistrictRepository.save(districts);
        return new ResponseEntity<>(_district, HttpStatus.CREATED);
    }

    public CDistrict updateDistrictById(int id, CDistrict districts) {
        Optional<CDistrict> districtData = pDistrictRepository.findById(id);
        if (districtData.isPresent()) {
			CDistrict _district= districtData.get();
			_district.setName(districts.getName());
			_district.setProvince(districts.getProvince());
			_district.setWards(districts.getWards());
            _district.setPrefix(districts.getPrefix());
            return _district;
        } else {
            return null;
        }
    }

    public ResponseEntity<CDistrict> deleteDistrictById(int id) {
        try {
			pDistrictRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    public ResponseEntity<CDistrict> deleteAllDistrict() {
		try {
			pDistrictRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
