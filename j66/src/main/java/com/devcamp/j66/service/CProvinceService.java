package com.devcamp.j66.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.j66.model.CProvince;
import com.devcamp.j66.repository.IProvinceRepository;

@Service
public class CProvinceService {
    @Autowired
    IProvinceRepository pIProvinceRepository;

    public List<CProvince> getAllProvince() {
        List<CProvince> provinces = new ArrayList<>();
        pIProvinceRepository.findAll().forEach(provinces::add);
        return provinces;
    }

    public CProvince getProvinceById(int id) {
        Optional<CProvince> prov = pIProvinceRepository.findById(id);
        if (prov.isPresent()) {
            CProvince province = prov.get();
            return province;
        } else {
            return null;
        }
    }

    public ResponseEntity<Object> postNewProvince(CProvince province) {
        Optional<CProvince> provinceData = pIProvinceRepository.findById(province.getId());
        if(provinceData.isPresent()) {
            return ResponseEntity.unprocessableEntity().body(" Province already exsit  ");
        }
        province.setCode(province.getCode());
        province.setName(province.getName());
        // province.setDistricts(province.getDistricts());
        CProvince _province = pIProvinceRepository.save(province);
        return new ResponseEntity<>(_province, HttpStatus.CREATED);
    }

    public CProvince updateProvinceById(int id, CProvince province) {
        Optional<CProvince> provinceData = pIProvinceRepository.findById(id);
        if (provinceData.isPresent()) {
			CProvince _province= provinceData.get();
			_province.setCode(province.getCode());
			_province.setName(province.getName());
			_province.setDistricts(province.getDistricts());
            return _province;
        } else {
            return null;
        }
    }

    public ResponseEntity<CProvince> deleteProvinceById(int id) {
        try {
			pIProvinceRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }

    public ResponseEntity<CProvince> deleteAllProvince() {
		try {
			pIProvinceRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
