package com.devcamp.j66.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j66.model.CDistrict;
import com.devcamp.j66.model.CProvince;
import com.devcamp.j66.model.CWard;
import com.devcamp.j66.repository.IDistrictRepository;
import com.devcamp.j66.repository.IProvinceRepository;
import com.devcamp.j66.repository.IWardRepository;
import com.devcamp.j66.service.CDistrictService;
import com.devcamp.j66.service.CProvinceService;
import com.devcamp.j66.service.CWardService;

@CrossOrigin
@RestController
@RequestMapping("/")
public class ApiController {
    
    @Autowired
    IProvinceRepository pIProvinceRepository;
    @Autowired
    IDistrictRepository pDistrictRepository;
    @Autowired
    IWardRepository pIWardRepository;
    @Autowired
    CProvinceService pCProvinceService;
    @Autowired
    CDistrictService pCDistrictService;
    @Autowired
    CWardService pCWardService;

    /*
     * API CRUD Province
     */
    // Phương thức GET all
    @GetMapping("/provinces")
    public List<CProvince> getAllProvince() {
        return pCProvinceService.getAllProvince();
    }
    // Phương thức GET by Id
    @GetMapping("/provinces/{id}")
    public ResponseEntity<CProvince> getProvinceById(@PathVariable("id") int id) {
        CProvince province = pCProvinceService.getProvinceById(id);
        if (province != null) {
			try {
				return new ResponseEntity<CProvince>(province,HttpStatus.OK);            
			} catch (Exception e) {
				return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);   
			}
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    // Phương thức PUT
    @PutMapping("/provinces/{id}")
    public ResponseEntity<Object> updateProvinceById(@PathVariable("id") int id, @RequestBody CProvince provinces) {
        CProvince province = pCProvinceService.updateProvinceById(id, provinces);
        if (province != null) {
            try {
                return new ResponseEntity<>(pIProvinceRepository.save(province), HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return ResponseEntity.unprocessableEntity().body("Failed to Update specified Province:"+e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Province: " + id + "  for update.");
        }
    }
    // Phương thức POST
    @PostMapping("/provinces")
    public ResponseEntity<Object> creatNewProvince(@RequestBody CProvince provinces) {
        try {
            return pCProvinceService.postNewProvince(provinces);
        } catch (Exception e) {
            //TODO: handle exception
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified Province: "+e.getCause().getCause().getMessage());
        }
    }
    // Phương thức DELETE by Id
    @DeleteMapping("/provinces/{id}")
    public ResponseEntity<CProvince> deleteProvinceById(@PathVariable("id") int id) {
        return pCProvinceService.deleteProvinceById(id);
    }
    // Phương thức DELETE all
    @DeleteMapping("/provinces")
    public ResponseEntity<CProvince> deleteAllProvince() {
        return pCProvinceService.deleteAllProvince();
    }

    /*
     * API CRUD District
     */
    // Phương thức GET all
    @GetMapping("/districts")
    public List<CDistrict> getAllDistrict() {
        List<CDistrict> districts = pCDistrictService.getAllDistrict();
        return districts;
    }
    // Phương thức GET by Id
    @GetMapping("/districts/{id}")
    public ResponseEntity<CDistrict> getDistrictById(@PathVariable("id") int id) {
        CDistrict district = pCDistrictService.getDistrictById(id);
        if (district != null) {
			try {
				return new ResponseEntity<CDistrict>(district,HttpStatus.OK);            
			} catch (Exception e) {
				return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);   
			}
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    // Phương thức PUT
    @PutMapping("/districts/{id}")
    public ResponseEntity<Object> updateDistrictById(@PathVariable("id") int id, @RequestBody CDistrict districts) {
        CDistrict district = pCDistrictService.updateDistrictById(id, districts);
        if (district != null) {
            try {
                return new ResponseEntity<>(pDistrictRepository.save(district), HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return ResponseEntity.unprocessableEntity().body("Failed to Update specified District:"+e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified District: " + id + "  for update.");
        }
    }
    // Phương thức POST
    @PostMapping("/districts")
    public ResponseEntity<Object> creatNewDistrict(@RequestBody CDistrict districts) {
        try {
            return pCDistrictService.postNewDistrict(districts);
        } catch (Exception e) {
            //TODO: handle exception
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified District: "+e.getCause().getCause().getMessage());
        }
    }
    // Phương thức DELETE by Id
    @DeleteMapping("/districts/{id}")
    public ResponseEntity<CDistrict> deleteDistrictById(@PathVariable("id") int id) {
        return pCDistrictService.deleteDistrictById(id);
    }
    // Phương thức DELETE all
    @DeleteMapping("/districts")
    public ResponseEntity<CDistrict> deleteAllDistrict() {
        return pCDistrictService.deleteAllDistrict();
    }

    /*
     * API CRUD Ward
     */
    // Phương thức GET all
    @GetMapping("/wards")
    public List<CWard> getAllWard() {
        return pCWardService.getAllWard();
    }
    // Phương thức GET by Id
    @GetMapping("/wards/{id}")
    public ResponseEntity<CWard> getWardById(@PathVariable("id") int id) {
        CWard ward = pCWardService.getWardById(id);
        if (ward != null) {
			try {
				return new ResponseEntity<CWard>(ward,HttpStatus.OK);            
			} catch (Exception e) {
				return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);   
			}
		} else {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
    }
    // Phương thức PUT
    @PutMapping("/wards/{id}")
    public ResponseEntity<Object> updateWardById(@PathVariable("id") int id, @RequestBody CWard wards) {
        CWard ward = pCWardService.updateWardById(id, wards);
        if (ward != null) {
            try {
                return new ResponseEntity<>(pIWardRepository.save(ward), HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return ResponseEntity.unprocessableEntity().body("Failed to Update specified Ward:"+e.getCause().getCause().getMessage());
            }
        } else {
            return ResponseEntity.badRequest().body("Failed to get specified Ward: " + id + "  for update.");
        }
    }
    // Phương thức POST
    @PostMapping("/wards")
    public ResponseEntity<Object> creatNewWard(@RequestBody CWard wards) {
        try {
            return pCWardService.postNewWard(wards);
        } catch (Exception e) {
            //TODO: handle exception
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified Ward: "+e.getCause().getCause().getMessage());
        }
    }
    // Phương thức DELETE by Id
    @DeleteMapping("/wards/{id}")
    public ResponseEntity<CWard> deleteWardById(@PathVariable("id") int id) {
        return pCWardService.deleteWardById(id);
    }
    // Phương thức DELETE all
    @DeleteMapping("/wards")
    public ResponseEntity<CWard> deleteAllWard() {
        return pCWardService.deleteAllWard();
    }
}
