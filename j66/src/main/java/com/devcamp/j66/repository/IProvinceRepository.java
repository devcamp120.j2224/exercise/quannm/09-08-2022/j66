package com.devcamp.j66.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j66.model.CProvince;

public interface IProvinceRepository extends JpaRepository<CProvince, Integer> {
}
