package com.devcamp.j66.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j66.model.CWard;

public interface IWardRepository extends JpaRepository<CWard, Integer> {
}
