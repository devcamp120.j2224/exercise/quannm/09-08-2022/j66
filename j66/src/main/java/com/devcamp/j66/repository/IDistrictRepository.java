package com.devcamp.j66.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j66.model.CDistrict;

public interface IDistrictRepository extends JpaRepository<CDistrict, Integer> {
}
